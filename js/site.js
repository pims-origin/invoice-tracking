new Vue({
    el: '#events-tracking-iframe',
    data() {
        return {
            events: [],
            isLoadingData: false,
            errMessage: '',
            isHideError: false,
            paginationObj: {},
            perPage: 20
        }
    },
    created: function () {
        this.fetchData(1);
    },
    methods: {
        fetchData: function (pageNum) {
            const that = this;
            const uri = window.location.search.substring(1);
            const urlParams = new URLSearchParams(uri);
            pageNum = pageNum ? pageNum : 1;
            const params = '?page=' + pageNum + '&limit=' + that.perPage;

            that.isLoadingData = true;
            $.ajax({
                method: 'GET',
                url: coreURL + '/invoice-master/v1/billableitem/search/refnum/' + urlParams.get('ref_num') + params,
                headers: {
                    "Authorization": "Bearer " + urlParams.get('token')
                },
                success: function (data) {
                    that.events = data.data;
                    that.isLoadingData = false;

                    // paging function
                    that.paginationObj = data['meta']['pagination'];
                    that.paginationObj['numLinks'] = 3;
                    that.paginationObj['tmpPageCount'] = that.processPagination(that.paginationObj);
                },
                error: function (err) {
                    that.errMessage = that.parseErrFromServer(err);
                    that.isLoadingData = false;
                }
            });
        },
        onPageSizeChanged: function($event) {
            this.perPage = $event.target.value;
            if(this.paginationObj){
                if((this.paginationObj['current_page'] - 1) * this.perPage >= this.paginationObj['total']) {
                    this.paginationObj['current_page'] = 1;
                }
                this.fetchData(this.paginationObj['current_page']);
            }
        },
        parseErrFromServer: function (errObj) {
            try {
                errObj = errObj.responseJSON.errors;
                try {
                    errMsg = JSON.parse(errObj.message).message;
                }
                catch (err) {
                    errMsg = errObj.message;
                }

                return errMsg;
            }
            catch (err) {
                return '';
            }
        },
        processPagination: function (data) {
            const pageCount = data['total_pages'];
            const currentPage = data['current_page'];
            const numLinks = data['numLinks'] ? data['numLinks'] : 3;
            let start = 1;
            let end = 1;
            let tmpPageCount = [];

            if (currentPage > numLinks) {
                start = currentPage - numLinks;
            } else {
                start = 1;
            }
            if (currentPage + numLinks < pageCount) {
                end = currentPage + numLinks;
            } else {
                end = pageCount;
            }
            //Create tmp for loop perPage in view
            for (var i = start; i <= end; i++) {
                tmpPageCount[i] = i;
            }

            tmpPageCount = tmpPageCount.filter(function (item) {
                return item !== undefined;
            });

            return tmpPageCount;
        }
    }
});